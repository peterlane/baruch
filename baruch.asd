(defsystem "baruch"
  :description "Writes Common Lisp documentation in asciidoc format."
  :version "0.2"
  :author "Peter Lane <peterlane@gmx.com>"
  :maintainer "Peter Lane <peterlane@gmx.com>"
  :homepage "https://peterlane.codeberg.page/baruch/"
  :licence "MIT"
  :components ((:file "src/package")
               (:file "src/baruch" :depends-on ("src/package")))
  :depends-on ("closer-mop" "trivial-arguments"))

