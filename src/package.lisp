(defpackage :baruch
  (:use :cl)
  (:export
    :document-package
    :document-system
    )
  (:documentation "Writes Common Lisp documentation in asciidoc format.

  [horizontal]
  documentation:: https://peterlane.codeberg.page/baruch[]
  sourcecode:: https://codeberg.org/peterlane/baruch[]

  Write package and API documentation in https://asciidoc.org/[asciidoc]
  format for every external symbol in a package into a single file.  This text
  you are reading is the package-level documentation for `baruch`
  with the API documentation given below under <<_functions>>.

  Text in documentation strings is copied directly to the output file
  (deindenting where necessary), and so you can use any of asciidoc's syntax.
  Notice the use in this package documentation of web links, description lists,
  formatted text and syntax highlighting of code. 
  
  The generated documentation file can then be converted to html or pdf,
  embedded in another document, or some sites (e.g. github) will render adoc
  files directly.  As the generated asciidoc text file is processed separately,
  documentation may include any extensions supported by the asciidoc converter
  program - for example, https://asciidoctor.org[asciidoctor] supports
  https://asciidoctor.org/docs/asciidoctor-diagram/[diagrams] and
  https://docs.asciidoctor.org/asciidoctor/latest/stem/mathematical/[mathematical
                                                                      syntax],
  amongst many others.

  To produce the documentation for this package:

  ....
  > (require :asdf)
  > (require :baruch)
  > (baruch:document-package :baruch \"baruch.adoc\")
  ....  

  For your exported symbols, write docstrings as usual, with optional asciidoc
  markup. For example, the docstring for <<document-package-function>> is:

  [source,lisp]
  ----
  (defun document-package (pkg-name filename &key ignore-defaults numbered toc)
    \"* `pkg-name` - name of package to document
  * `filename` - name of file to save package information into
  * `ignore-defaults` - t/nil flag to not write the default values for key/optional parameters
  * `numbered` - t/nil flag to number the sections (includes :sectnums: attribute)
  * `toc` - t/nil flag to include table of contents (includes :toc: attribute)

  Writes information on all exported symbols in given package as an asciidoc file.
  
  Error:: if `pkg-name` is not found.\"
      ; <SNIP> IMPLEMENTATION
    )
  ----

  Baruch intentionally makes no attempt to link package and system
  documentation together into single 'web sites' or 'pdf books', leaving such
  arrangements to the documenter. 

  === Install

  To install `baruch`, download the 
  https://codeberg.org/peterlane/baruch/releases[latest release]
  and make the code available to the
  https://common-lisp.net/project/asdf/asdf/Configuring-ASDF-to-find-your-systems.html[asdf]
  system.

  [horizontal]
  Depends on:: https://github.com/pcostanza/closer-mop[closer-mop] and 
               https://github.com/Shinmera/trivial-arguments[trivial-arguments]

  (Baruch should work with Common Lisp implementations supported by
  https://github.com/pcostanza/closer-mop[closer-mop] - tested on ABCL, CLISP and SBCL.)

  === Examples

  1. You can use `baruch` with any package: for example 
  https://github.com/pcl-code/ltk/blob/documentation/doc/ltk.adoc[LTk documentation] -
  automatically rendered by Github. 
  2. The documentation for _this_ package is an example where the documentation
  strings include asciidoc markup.
  3. Asciidoc is accepted in some static-site generators, such as
     https://jekyllrb.com/[jekyll]. 

  === Symbol Anchors, Cross References and Links

  Symbols are listed under their respective group: class, function, macro,
  type, or variable.

  Each symbol is given an anchor which can be used to create cross-references
  within documentation strings or across different parts of the documentation.
  Each anchor is of the form NAME-GROUP, where NAME is the symbol name and
  GROUP is one of the above groups.

  Symbol names have asterisks and plus symbols removed in the link anchor, so
  the documentation for the exported variable \\*tk* is linked to as `<<
  tk-variable >>`.

  Symbols can then be cross-referenced using the anchor using normal asciidoc syntax.
  For example, to link to baruch's <<document-package-function>> function:

  * internally, from within another docstring in the same package (as above!): 
  [literal]
  <<document-package-function>>
  * internally, from another page in the same website: 
  [literal]
  link:/baruch/#document-package-function[document-package]
  * externally, from a page in a different website:
  [literal]
  https://peterlane.codeberg.page/baruch/#document-package-function[document-package]
  "))

