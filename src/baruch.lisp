;; TODO
;; - print slot documentation along with slot name

(in-package :baruch)

(defun document-package (pkg-name filename &key ignore-defaults numbered toc)
  "* `pkg-name` - name of package to document
  * `filename` - name of file to save package information into
  * `ignore-defaults` - t/nil flag to not write the default values for key/optional parameters
  * `numbered` - t/nil flag to number the sections (includes :sectnums: attribute)
  * `toc` - t/nil flag to include table of contents (includes :toc: attribute)

  Writes information on all exported symbols in given package as an asciidoc file.
  
  Error:: if `pkg-name` is not found."
  (let ((pkg (find-package pkg-name))
        (sym-types '())
        (sym-classes '())
        (sym-variables '())
        (sym-macros '())
        (sym-functions '()))
    (unless (packagep pkg)
      (error "Package ~a is not found" pkg-name))
    ; extract symbols into groups
    (do-external-symbols (sym (find-package pkg))
      (when (boundp sym)
        (push sym sym-variables))
      (when (fboundp sym)
        (if (macro-function sym)
          (push sym sym-macros)
          (push sym sym-functions)))
      (when (find-class sym nil)
        (push (find-class sym) sym-classes))
      (when (and (documentation sym 'type)
                 (not (find-class sym nil)))
        (push sym sym-types)))
    ; write information to file
    (uiop:call-with-output-file 
      filename
      (lambda (str)
        (format str "= Package ~a" (package-name pkg))
        (when numbered
          (format str "~%:sectnums:"))
        (when toc 
          (format str "~%:toc:"))
        (format str "~%~%[horizontal]~%")
        (format str "Package name:: ~a~%" (package-name pkg))
        (when (package-nicknames pkg)
          (format str "Nicknames:: ~{~a ~}~%" (package-nicknames pkg)))
        (format str "~%")
        ; -- output package level information
        (format str "== Package documentation~%~%")
        (format str "~a" (deindent-documentation (find-package pkg) t))
        (format str "~%~%")
        (when sym-classes
          (format str "== Classes~%~%")
          (dolist (sym (sort sym-classes #'string< :key #'class-name))
            (write-class str sym)))
        (when sym-functions
          (format str "== Functions~%~%")
          (dolist (sym (sort sym-functions #'string<))
            (write-function str sym ignore-defaults)))
        (when sym-macros
          (format str "== Macros~%~%")
          (dolist (sym (sort sym-macros #'string<))
            (write-macro str sym ignore-defaults)))
        (when sym-types
          (format str "== Types~%~%")
          (dolist (sym (sort sym-types #'string<))
            (write-type str sym)))
        (when sym-variables
          (format str "== Variables~%~%")
          (dolist (sym (sort sym-variables #'string<))
            (write-variable str sym)))
        )
      :if-exists :supersede)))

(defun document-system (system-name filename)
  "* `system-name` - name of system to document
  * `filename` - name of file to save system information into

  Writes system-level information into given file in asciidoc format, including, 
  where available: name, author, maintainer, homepage, license, version, 
  depends-on, description and long-description.

  Error:: if `system-name` is not found."
  (let ((system-definition (asdf:find-system system-name)))
    (when system-definition
      (uiop:call-with-output-file 
        filename
        (lambda (str)
          (format str "= System ~a~%~%" (asdf:component-name system-definition))
          (format str "~a~%~%" (asdf:system-description system-definition)) 
          (format str "[horizontal]~%")
          (when (asdf:system-author system-definition)
            (format str "Author:: ~a~%" (asdf:system-author system-definition)))
          (when (and (asdf:system-maintainer system-definition)
                     (not (string= (asdf:system-author system-definition)
                                   (asdf:system-maintainer system-definition))))
            (format str "Maintainer:: ~a~%" (asdf:system-maintainer system-definition)))
          (when (asdf:system-homepage system-definition)
            (format str "Homepage:: ~a~%" (asdf:system-homepage system-definition)))
          (when (asdf:system-license system-definition)
            (format str "License:: ~a~%" (asdf:system-license system-definition)))
          (when (asdf:component-version system-definition)
            (format str "Version:: ~a~%" (asdf:component-version system-definition)))
          (when (asdf:system-depends-on system-definition)
            (format str "Depends on:: ~a~%" (asdf:system-depends-on system-definition)))

          (format str "~%")
          (when (asdf:system-long-description system-definition)
            (format str "~a~%" (asdf:system-long-description system-definition)))
          )
        :if-exists :supersede))))

;; --------------------------------------------------------------------------
;; -- internal functions

(defun left-margin (strs)
  (let ((margins 
          (remove nil
                  (mapcar #'(lambda (str) (position-if-not #'(lambda (c) (eq c #\space)) str))
                          strs))))
    (if margins
      (reduce #'min margins)
      0)))

(defun deindent-documentation (sym kind)
  (deindent-docstring (documentation sym kind)))

(defun deindent-docstring (doc-str)
  (let* ((lines (uiop:split-string doc-str :separator (string #\newline)))
         (indent (left-margin (rest lines))))
    (format nil "~{~a~%~}"
            (cons (first lines)
                  (mapcar #'(lambda (line) ; remove initial spaces from each line
                              (if (> (length line) indent) ; if line is long enough
                                (subseq line indent)
                                line))
                          (rest lines))))))

(defun write-class (str sym)
  (write-sym str (class-name sym) "class")
  ; Class details depend on features in implementation
  ; - relies on closer-mop for compatibility
  (closer-mop:finalize-inheritance sym)
  (when (closer-mop:class-precedence-list sym)
    (format str "Super classes:: ~{~a ~}~%~%" 
            (remove (class-name sym)
                    (mapcar #'class-name (closer-mop:class-precedence-list sym)))))
  (when (closer-mop:class-direct-subclasses sym)
    (format str "Child classes:: ~{~a ~}~%~%" 
            (remove (class-name sym)
                    (mapcar #'class-name (closer-mop:class-direct-subclasses sym)))))
  (when (closer-mop:class-direct-slots sym)
    (format str "Slots::~%~%")
    (dolist (slot (closer-mop:class-direct-slots sym))
      (format str "* `~(~a~)`~%" (closer-mop:slot-definition-name slot)))
    (format str "~%//-~%~%"))
  (format str "Documentation::~&~a~%~%" (deindent-documentation sym t)))

(defun write-function (str sym ignore-defaults)
  (write-sym str sym "function")
  (write-lambda-list str sym (arg:arglist (symbol-function sym)) ignore-defaults)
  (format str "Documentation::~&~a~%~%~%" (deindent-documentation sym 'function)))

(defun write-macro (str sym ignore-defaults)
  (write-sym str sym "macro")
  (write-lambda-list str sym (arg:arglist (macro-function sym)) ignore-defaults t)
  (format str "Documentation::~&~a~%~%" (deindent-documentation sym 'function)))

(defun write-type (str sym)
  (write-sym str sym "type")
  (format str "Documentation::~&~a~%~%" (deindent-documentation sym 'type)))

(defun write-variable (str sym)
  (write-sym str sym "variable")
  (format str "Documentation::~&~a~%~%" (deindent-documentation sym 'variable)))

(defun escape-sym (sym)
  "Lisp symbol names can include asciidoc significant chars, like *x*. 
  These need to be escaped. We will only handle first * and +."
  (setf sym (string sym))
  (let ((first-star (position #\* sym)))
    (when first-star
      (setf sym
            (format nil "~a\\~a" (subseq sym 0 first-star) (subseq sym first-star)))))
  (let ((first-plus (position #\+ sym)))
    (if first-plus
      (format nil "~a\\~a" (subseq sym 0 first-plus) (subseq sym first-plus))
      sym)))

(defun write-lambda-list (str sym lambda-list &optional (ignore-defaults nil) (is-macro nil))
  "Writes given lambda list"
  (unless (eql lambda-list :unknown) ; do not write empty lambda list
    (format str "Lambda list:: ~(~a~) ( ~{~a ~})~%~%"
            (escape-sym sym)
            (mapcar #'(lambda (arg)
                        (let ((arg-str (format nil "~(~a~)" arg)))
                          (if (member arg-str '("&body" "&key" "&optional" "&rest") :test #'string=)
                            arg-str
                            (format nil "*~a*" arg-str))))
                    (mapcar #'(lambda (arg) (if (and (not is-macro)  ; only for functions currently
                                                     ignore-defaults ; try to ignore default values
                                                     (listp arg))
                                              (first arg) ; by taking the first of any pair
                                              arg))
                            lambda-list)))))

(defun write-sym (str sym sym-type)
  "Writes symbol anchor and name as an asciidoc title"
  (format str "[[~(~a~)-~a]]~&" 
          (remove #\+ (remove #\* (string sym)))
          sym-type)
  (format str ".~a~%~%" (escape-sym sym)))

